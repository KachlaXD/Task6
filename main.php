<!DOCTYPE HTML>  
<html>
<head>
</head>
<body>  

<?php

$name = $title = $comment =  "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = test_input($_POST["name"]); //ტესტავს შეყვანილ მონაცემებს
  $title = test_input($_POST["title"]);
  $comment = test_input($_POST["comment"]);
}

function test_input($data) {
  $data = trim($data); //პირველი 2 და ბოლო 2 სიმბოლოს მიხედვით ვალიდაცია
  $data = stripslashes($data);//ვალიდაცია არის თუ არა სავსე ველი
  $data = htmlspecialchars($data);//ეს დავაკოპირე კარგად არვიცი რასაკეთებს 
  return $data;
}
?>

<h2>Task 2</h2>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  სახელი და გვარი: <input type="text" name="name">
  <br><br>
  სათაური: <input type="text" name="title">
  <br><br>
  სტატია: <textarea name="comment" rows="5" cols="40"></textarea>
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>


<?php
$txt = "data.txt";
if (isset($_POST['name']) && isset($_POST['title']) && isset($_POST['comment'])){
    $chawera = fopen($txt, 'a'); 
    $txt=$_POST['name'].' - '.$_POST['title'] . ' - '. $_POST['comment']; 
    fwrite($chawera,$txt);
    fclose($chawera);
}
?>
</body>
</html>